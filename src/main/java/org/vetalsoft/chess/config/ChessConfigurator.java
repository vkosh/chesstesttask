package org.vetalsoft.chess.config;

import org.vetalsoft.chess.context.ChessContext;
import org.vetalsoft.chess.exception.ConfigurationException;
import org.vetalsoft.chess.util.Algorithm;
import org.vetalsoft.chess.util.Constants;
import org.vetalsoft.chess.util.FigureType;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for configuration
 */
public class ChessConfigurator {

    /*====================================================================================================*/
    /*========================================= FIELDS ===================================================*/
    /*====================================================================================================*/
    
    private static Properties figuresProperties = new Properties();
    private static Properties projectProperties = new Properties();

    private static Logger _logger = Logger.getLogger(ChessConfigurator.class.getName());

    /*====================================================================================================*/
    /*======================================== STATIC INITIALIZATION =====================================*/
    /*====================================================================================================*/
    static {
        
        //Initialize properties for figures configuration
        try {
            //Load 'configuration.properties' with figures configuration
            figuresProperties.load(ChessConfigurator.class.getResourceAsStream("/configuration.properties"));
            //Validate configuration
            validateFiguresConfiguration();
        } catch (Exception e){
            String errorMessage = "Can not load figure settings from properties file: " + e.getMessage();
            _logger.warning(errorMessage);
            throw new ConfigurationException(errorMessage);
        }

        //Initialize properties for project settings
        try {
            //Load 'settings.properties' file with project settings
            projectProperties.load(ChessConfigurator.class.getResourceAsStream("/settings.properties"));
            //Validate configuration
            validateProjectConfiguration();
        } catch (Exception e){
            String errorMessage = "Can not load project settings from properties file: " + e.getMessage();
            _logger.warning(errorMessage);
            throw new ConfigurationException(errorMessage);
        }
    }

    /*====================================================================================================*/
    /*=========================================== PUBLIC METHODS =========================================*/
    /*====================================================================================================*/

    /**
     * Configures project.
     * Reads configuration from config files and sets it to application context.
     */
    public static void configure()  {
        
        //Read figures configuration 
        Map<FigureType, String> figuresSettings = new HashMap<FigureType, String>();
        for (FigureType figureType : FigureType.values()) {
            String propertyName = "figure." + figureType + ".beaten.cells";
            String figureConfig = figuresProperties.getProperty(propertyName);
            figuresSettings.put(figureType, figureConfig);
        }

        //Read 'chess.algorithm' property from project configuration file 
        String algorithmProperty = projectProperties.getProperty("chess.algorithm");
        Algorithm algorithm = Algorithm.valueOf(algorithmProperty);

        try {
            /**
             * Set figure settings and algorithm to ChessContext.
             * 
             * This is done via reflection API, because those methods are private, 
             * as ChessContext API allows only to get values from it.
             * This is the only place, where values are set to ChessContext. 
             */
        
            //Set figure settings to ChessContext
            Method setFiguresSettingsMethod = ChessContext.class.getDeclaredMethod("setFiguresSettings", Map.class);
            setFiguresSettingsMethod.setAccessible(true);
            setFiguresSettingsMethod.invoke(null, figuresSettings);

            //Set algorithm to ChessContext
            Method setAlgorithmMethod = ChessContext.class.getDeclaredMethod("setAlgorithm", Algorithm.class);
            setAlgorithmMethod.setAccessible(true);
            setAlgorithmMethod.invoke(null, algorithm);

        } catch (Exception e) {
            //Handle exception
            String errorMessage = "Error while initializing chess context: " + e.getMessage();
            _logger.warning(errorMessage);
            throw new ConfigurationException(e);
        }
    }

    /*====================================================================================================*/
    /*========================================== PRIVATE METHODS =========================================*/
    /*====================================================================================================*/

    /**
     * Validates figures configuration
     */
    private static void validateFiguresConfiguration() {
        for (FigureType figureType : FigureType.values()) {
            String propertyName = "figure." + figureType + ".beaten.cells";
            String figureConfig = figuresProperties.getProperty(propertyName);
            if (figureConfig == null) {
                throw new ConfigurationException("No configuration found for figure: " + figureType);
            }
            String[] configOptions = figureConfig.split(";");
            for (String configOption : configOptions) {
                configOption = configOption.trim();
                if (Constants.LINES.equals(configOption) || Constants.DIAGONALS.equals(configOption)) {
                    continue;
                }
                String[] configIndexes = configOption.split(",");
                if (configIndexes.length != 2) {
                    throw new ConfigurationException("Invalid configuration option '" + configOption + "' for figure: " + figureType);
                }
                try {
                    Integer rowIndex = Integer.valueOf(configIndexes[0].trim());
                    Integer columnIndex = Integer.valueOf(configIndexes[1].trim());
                } catch (Exception e) {
                    throw new ConfigurationException("Invalid configuration option '" + configOption + "' for figure: " + figureType);
                }
            }
        }
        
    }

    /**
     * Validates project configuration
     */
    private static void validateProjectConfiguration() {
        String algorithmProperty = projectProperties.getProperty("chess.algorithm");
        if (!Algorithm.ITERATION.name().equals(algorithmProperty) && !Algorithm.ROTATION.name().equals(algorithmProperty)) {
            throw new ConfigurationException("Algorithm not supported: '" + algorithmProperty + "'.");
        }
    }

}
