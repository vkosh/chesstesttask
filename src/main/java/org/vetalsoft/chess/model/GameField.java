package org.vetalsoft.chess.model;

import org.vetalsoft.chess.util.Constants;
import org.vetalsoft.chess.util.FigureType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for game field
 *
 */
public class GameField {
    
    /*====================================================================================================*/
    /*========================================= FIELDS ===================================================*/
    /*====================================================================================================*/
    
    private int columns;
    
    private int rows;
    
    private int[][] gameArray;
    
    /*====================================================================================================*/
    /*========================================= CONSTRUCTORS =============================================*/
    /*====================================================================================================*/
    
    public GameField() {
    }

    public GameField(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        initialize();
    }

    public GameField(int rows, int columns, int[][]gameArray) {
        this.rows = rows;
        this.columns = columns;
        this.gameArray =  new int[rows][columns];
        for (int row = 0; row < rows; row++) {
            this.gameArray[row] = gameArray[row].clone();
        }
    }
    
    /*====================================================================================================*/
    /*========================================= METHODS ==================================================*/
    /*====================================================================================================*/

    /**
     * @see org.vetalsoft.chess.util.Constants#EMPTY
     *
     * Initializes gameArray with default values  
     */
    private void initialize(){
        gameArray = new int[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                gameArray[row][column] = Constants.EMPTY;
            }
        }
        
    }


    /**
     * Prints current state to console
     * */
    public void print() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int value = gameArray[i][j];
                String printValue = value + " ";
                FigureType figureType = FigureType.getByIndex(value);
                if (figureType != null) {
                    printValue = figureType.getLabel() + " ";
                }
                System.out.print(printValue);
            }
            System.out.println();
        }
    }

    /**
     * Sets value for cell, specified by row/column position 
     *    
     * @param row - row position
     * @param column - column position
     * @param value - value to be set
     */
    public void setValue(int row, int column, int value) {
        if (isValidIndex(row, column)) {
            gameArray[row][column] = value;
        }
    }

    /**
     * Returns value for cell, specified by row/column position
     *
     * @param row - row position
     * @param column - column position
     * @return cell vale
     */
    public int getCellValue(int row, int column){
        if (isValidIndex(row, column)) {
            return gameArray[row][column];
        }
        return 0;
    }


    /**
     * Returns list of free cells
     *
     * @return List of free cells
     */
    public List<Cell> getFreeCells(){
        List<Cell> freeCells = new ArrayList<Cell>();
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                if (gameArray[row][column] == Constants.EMPTY) {
                    freeCells.add(new Cell(row, column));
                }
            }
        }
        return freeCells;
    }

    /**
     * Returns value for cell
     * 
     * @param cell - Cell
     * @return cell value
     */
    public int getCellValue(Cell cell) {
        return getCellValue(cell.getRow(), cell.getColumn());
    }


    /**
     * Checks if cell is free (no figure placed on it)
     * @param cell - Cell
     * @return - true,  if cell is free
     */
    public boolean isFree(Cell cell) {
        Integer cellValue = getCellValue(cell);
        return FigureType.getByIndex(cellValue) == null;
    }
    
    /**
     * Checks if indexes are valid (to avoid ArrayIndexOfBound)
     *
     * @param row - row position
     * @param column - column position
     * 
     * @return - true, if indexes valid
     */
    public boolean isValidIndex(int row, int column){
        return row >= 0 && column >= 0 && row < this.rows && column < this.columns;
    }

    /*====================================================================================================*/
    /*====================================== EQUALS/HASHCODE =============================================*/
    /*====================================================================================================*/
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameField)) return false;
        GameField gameField = (GameField) o;
        if (columns != gameField.columns) return false;
        if (rows != gameField.rows) return false;
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                if (gameArray[row][column] != gameField.gameArray[row][column]) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = columns;
        result = 31 * result + rows;
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                int hashValue = (gameArray[row][column] * 13 + row * 17) * 7 + column * 11;
                result += hashValue;
            }
        }
        return result;
    }

    /*====================================================================================================*/
    /*====================================== GETTERS/SETTERS =============================================*/
    /*====================================================================================================*/
    
    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int[][] getGameArray() {
        return gameArray;
    }

    public void setGameArray(int[][] gameArray) {
        this.gameArray = gameArray;
    }
}
