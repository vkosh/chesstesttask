package org.vetalsoft.chess.model;


/**
 * @author - Vitaliy Koshelenko
 *
 * Class for cell
 *
 */
public class Cell {
    
    /*====================================================================================================*/
    /*========================================= FIELDS ===================================================*/
    /*====================================================================================================*/

    private int row;        //row index
    private int column;     //column index

    /*====================================================================================================*/
    /*========================================= CONSTRUCTORS =============================================*/
    /*====================================================================================================*/

    /**
     * Default constructor 
     */    
    public Cell() {
    }

    /**
     * Constructor with parameters
     *
     * @param row - row index
     * @param column - column index
     */
    public Cell(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /*====================================================================================================*/
    /*=================================== EQUALS/HASHCODE ================================================*/
    /*====================================================================================================*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cell)) return false;

        Cell cell = (Cell) o;

        if (column != cell.column) return false;
        if (row != cell.row) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + column;
        return result;
    }

    /*====================================================================================================*/
    /*=================================== GETTERS/SETTERS ================================================*/
    /*====================================================================================================*/
    
    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

}
