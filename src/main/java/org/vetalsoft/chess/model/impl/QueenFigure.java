package org.vetalsoft.chess.model.impl;

import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.util.FigureType;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for queen chess figure
 */
public class QueenFigure extends Figure {

    /**
     * Returns figure type for QUEEN
     *
     * @see org.vetalsoft.chess.util.FigureType#QUEEN
     *
     * @return - QUEEN FigureType
     */
    @Override
    public FigureType getFigureType() {
        return FigureType.QUEEN;
    }
}
