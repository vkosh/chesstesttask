package org.vetalsoft.chess.model.impl;

import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.util.FigureType;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for officer chess figure
 */
public class OfficerFigure extends Figure {


    /**
     * Returns figure type for OFFICER
     *
     * @see org.vetalsoft.chess.util.FigureType#OFFICER
     *
     * @return - OFFICER FigureType
     */
    @Override
    public FigureType getFigureType() {
        return FigureType.OFFICER;
    }
}
