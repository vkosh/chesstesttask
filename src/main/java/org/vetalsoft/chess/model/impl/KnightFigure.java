package org.vetalsoft.chess.model.impl;

import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.util.FigureType;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for knight chess figure
 */
public class KnightFigure extends Figure {

    /**
     * Returns figure type for KNIGHT
     *
     * @see org.vetalsoft.chess.util.FigureType#KNIGHT
     *
     * @return - KNIGHT FigureType
     */
    @Override
    public FigureType getFigureType() {
        return FigureType.KNIGHT;
    }
}
