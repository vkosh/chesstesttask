package org.vetalsoft.chess.model.impl;

import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.util.FigureType;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for king chess figure
 */
public class KingFigure extends Figure {

    /**
     * Returns figure type for KING
     * 
     * @see org.vetalsoft.chess.util.FigureType#KING
     *    
     * @return - KING FigureType
     */
    @Override
    public FigureType getFigureType() {
        return FigureType.KING;
    }
}
