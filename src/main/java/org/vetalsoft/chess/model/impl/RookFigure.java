package org.vetalsoft.chess.model.impl;

import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.util.FigureType;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for rook chess figure
 */
public class RookFigure extends Figure {

    /**
     * Returns figure type for ROOK
     *
     * @see org.vetalsoft.chess.util.FigureType#ROOK
     *
     * @return - ROOK FigureType
     */
    @Override
    public FigureType getFigureType() {
        return FigureType.ROOK;
    }
}
