package org.vetalsoft.chess.model;

import org.vetalsoft.chess.context.ChessContext;
import org.vetalsoft.chess.service.figure.FigureServiceUtil;
import org.vetalsoft.chess.util.FigureType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author - Vitaliy Koshelenko
 *
 * Base class for chess figure
 *
 */
public abstract class Figure {

    /*====================================================================================================*/
    /*=========================================== ABSTRACT METHODS =======================================*/
    /*====================================================================================================*/
    
    /**
     * @see org.vetalsoft.chess.util.FigureType
     * Returns FigureType value for figure. 
     * Implemented in child classes.   
     *
     * @return - cell value for figure
     */
    public abstract FigureType getFigureType();
    
    /*====================================================================================================*/
    /*=========================================== PUBLIC METHODS =========================================*/
    /*====================================================================================================*/

    /**
     * Calculates List of beaten cells for figure
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position
     *
     * @return - List of beaten cells
     */
    public List<Cell> getBeatenCells(GameField gameField, int row, int column) {

        //create list of beaten cells
        List<Cell> beatenCells = new ArrayList<Cell>();

        //get configuration options for figure
        String figureConfiguration = ChessContext.getFigureConfiguration(getFigureType());

        //iterate over each configuration option and add beaten cells for it to the list
        String[] configurationOptions = figureConfiguration.split(";");
        for (String configurationOption : configurationOptions) {
            List<Cell> beatenCellsPart = FigureServiceUtil.getBeatenCells(gameField, row, column, configurationOption);
            beatenCells.addAll(beatenCellsPart);
        }

        //return list of beaten cells
        return beatenCells;
    }

    /**
     * Fills beaten cells for figure
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position               
     */
    public void fillBeatenCells(GameField gameField, int row, int column) {

        String figureConfiguration = ChessContext.getFigureConfiguration(getFigureType());
        String[] configurationOptions = figureConfiguration.split(";");
        for (String configurationOption : configurationOptions) {
            FigureServiceUtil.fillBeatenCells(gameField, row, column, configurationOption);
        }

    }

    /**
     * Checks if figure can be placed at specified row/column position 
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position
     *
     * @return - true, if figure can be placed
     */
    public boolean canBePlaced(GameField gameField, int row, int column) {
        List<Cell> beatenCells = getBeatenCells(gameField, row, column);
        for (Cell cell : beatenCells) {
            if (!gameField.isFree(cell)) {
                //other figure is placed on beaten cell, can not place figure here
                return false;
            }
        }
        return true;
    }
    
}
                  