package org.vetalsoft.chess;

import org.vetalsoft.chess.config.ChessConfigurator;
import org.vetalsoft.chess.util.Constants;
import org.vetalsoft.chess.processor.ChessProcessor;
import org.vetalsoft.chess.util.FigureType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * @author - Vitaliy Koshelenko
 *
 * Main class for Chess Test Task
 *
 */
public class Main {

    public static void main(String[] args) {

        //read configuration from properties
        ChessConfigurator.configure();

        //create BufferedReader instance to read from console
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        //number of rows
        int rows = readFromConsole(br, Constants.MSG_ROW_COUNT, Constants.MIN_SIZE, Constants.MAX_SIZE);
        //number of columns
        int columns = readFromConsole(br, Constants.MSG_COL_COUNT, Constants.MIN_SIZE, Constants.MAX_SIZE);

        /**
         * Map with figures count.
         * Key - figure type (@see org.vetalsoft.chess.model.Constants.ALL_FIGURES)
         * Value - number of pieces of figure type
         */
        Map<FigureType,Integer> figuresCountMap = new HashMap<FigureType, Integer>();
        
        //number of pieces of kings
        int kingCount = readFromConsole(br, Constants.MSG_KING_COUNT, Constants.MIN_FIGURES, Constants.MAX_FIGURES);
        figuresCountMap.put(FigureType.KING, kingCount);
        
        //number of pieces of rooks
        int rookCount = readFromConsole(br, Constants.MSG_ROOKS_COUNT, Constants.MIN_FIGURES, Constants.MAX_FIGURES);
        figuresCountMap.put(FigureType.ROOK, rookCount);
        
        //number of pieces of knights
        int knightCount = readFromConsole(br, Constants.MSG_KNIGHTS_COUNT, Constants.MIN_FIGURES, Constants.MAX_FIGURES);
        figuresCountMap.put(FigureType.KNIGHT, knightCount);
        
        //number of pieces of officers
        int officersCount = readFromConsole(br, Constants.MSG_OFFICERS_COUNT, Constants.MIN_FIGURES, Constants.MAX_FIGURES);
        figuresCountMap.put(FigureType.OFFICER, officersCount);
        
        //number of pieces of queens
        int queensCount = readFromConsole(br, Constants.MSG_QUEENS_COUNT, Constants.MIN_FIGURES, Constants.MAX_FIGURES);
        figuresCountMap.put(FigureType.QUEEN, queensCount);
        
        //Crate ChessProcessor instance
        ChessProcessor chessProcessor = new ChessProcessor(rows, columns, figuresCountMap);
        //process and display results
        chessProcessor.process();
        
        //safely close BufferedReader
        safeClose(br);
    }

    /**
     * Reads int value from console
     *  
     * @param br - BufferedReader
     * @param message - display message
     * @param min - minimum value
     * @param max - maximum value
     *
     * @return value from console
     */
    private static int readFromConsole(BufferedReader br, String message, int min, int max) {
        int value = -1;
        do {
            try {
                System.out.print(message);
                String columnsString = br.readLine();
                value = Integer.parseInt(columnsString);
            } catch (Exception e) {
                System.out.println("Error: '" + e.getMessage() + "'. Please, try again.");
            }
            if (value > max || value < min) {
                System.out.println("Error: value must be between '" + min + "' and '" + max + "'");
            }
        } while (value < min || value > max);
        return value;
    }


    /**
     * Safely closes the BufferedReader
     *
     * @param br - BufferedReader
     */
    private static void safeClose(BufferedReader br) {
        try {
            br.close();
        } catch (IOException ignored) {
        }
    }
}
