package org.vetalsoft.chess.processor;

import org.vetalsoft.chess.factory.CombinationServiceFactory;
import org.vetalsoft.chess.factory.FigureFactory;
import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.model.GameField;
import org.vetalsoft.chess.service.combination.CombinationsService;
import org.vetalsoft.chess.util.FigureType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author - Vitaliy Koshelenko
 * <p/>
 * Class for finding unique configurations
 */
public class ChessProcessor {

    /*====================================================================================================*/
    /*========================================= FIELDS ===================================================*/
    /*====================================================================================================*/

    //rows count on board
    private int rows;

    //columns count on board
    private int columns;

    /**
     * Map with figures count.
     * Key - figure type (@see org.vetalsoft.chess.model.Constants.ALL_FIGURES)
     * Value - number of pieces of figure type
     */
    private Map<FigureType, Integer> figuresCountMap;

    /*====================================================================================================*/
    /*========================================= CONSTRUCTORS =============================================*/
    /*====================================================================================================*/

    /**
     * Default constructor
     */
    public ChessProcessor() {
    }

    /**
     * Constructor with parameters
     *
     * @param rows            - number of rows
     * @param columns         - number of columns
     * @param figuresCountMap - map with figures count. Key - figure type, value - number of pieces of figure type
     */
    public ChessProcessor(int rows, int columns, Map<FigureType, Integer> figuresCountMap) {
        this.rows = rows;
        this.columns = columns;
        this.figuresCountMap = figuresCountMap;
    }

    /*====================================================================================================*/
    /*========================================= METHODS ==================================================*/
    /*====================================================================================================*/


    /**
     * Finds set of combinations and prints to output
     */
    public void process() {

        long startTime = System.currentTimeMillis();

        //Initialize list of figures
        List<Figure> figures = new ArrayList<Figure>();

        //fill figures list with figures
        for (FigureType figureType : figuresCountMap.keySet()) {
            int figuresCount = figuresCountMap.get(figureType);
            for (int counter = 0; counter < figuresCount; counter++) {
                Figure figure = FigureFactory.getFigure(figureType);
                figures.add(figure);
            }
        }

        
        //Get CombinationService instance
        CombinationsService combinationsService = CombinationServiceFactory.getCombinationsService();
        //Calculate set of combinations
        Set<GameField> combinations = combinationsService.getCombinations(rows, columns, figures);

        if (combinations.size() == 0) {
            System.out.println("No combinations available.");
        }

        //Print combinations to output
        int counter = 0;
        for (GameField combination : combinations) {
            System.out.println("Combination " + ++counter + ": ");
            combination.print();
            System.out.println();
        }
        System.out.println("End.");

        long timeInMills = System.currentTimeMillis() - startTime;
        System.out.println("Run time: " + timeInMills + "ms.");
    }

    /*====================================================================================================*/
    /*====================================== GETTERS/SETTERS =============================================*/
    /*====================================================================================================*/

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public Map<FigureType, Integer> getFiguresCountMap() {
        return figuresCountMap;
    }

    public void setFiguresCountMap(Map<FigureType, Integer> figuresCountMap) {
        this.figuresCountMap = figuresCountMap;
    }
}
