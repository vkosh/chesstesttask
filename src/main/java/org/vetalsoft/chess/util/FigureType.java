package org.vetalsoft.chess.util;

/**
 * Enum for figure type.
 * 
 * Possible values:
 *  - org.vetalsoft.chess.util.FigureType#KING - KING; 
 *  - org.vetalsoft.chess.util.FigureType#ROOK - ROOK; 
 *  - org.vetalsoft.chess.util.FigureType#KNIGHT - KNIGHT; 
 *  - org.vetalsoft.chess.util.FigureType#OFFICER - OFFICER; 
 *  - org.vetalsoft.chess.util.FigureType#QUEEN - QUEEN. 
 */
public enum FigureType {

    KING(2, "K"), ROOK(3, "R"), KNIGHT(4, "N"), OFFICER(5, "O"), QUEEN(6, "Q");

    private Integer figureIndex; //figure index
    private String label;        //figure label

    /**
     * Returns figure type by figure index. 
     *    
     * @param figureIndex - figure index.
     * @return - figure type.
     */
    public static FigureType getByIndex(int figureIndex){
        FigureType figureType;
        switch (figureIndex) {
            case 2: figureType = KING;
                    break;
            case 3: figureType = ROOK;
                    break;
            case 4: figureType = KNIGHT;
                    break;
            case 5: figureType = OFFICER;
                    break;
            case 6: figureType = QUEEN;
                    break;
            default: figureType = null;
                break;
        }
        return figureType;
    }

    /**
     * Constructor with parameters
     *
     * @param figureType - figure type
     * @param label - figure label
     */
    FigureType(Integer figureType, String label) {
        this.figureIndex = figureType;
        this.label = label;
    }

    public Integer getFigureIndex() {
        return figureIndex;
    }

    public void setFigureIndex(Integer figureIndex) {
        this.figureIndex = figureIndex;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
