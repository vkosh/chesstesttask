package org.vetalsoft.chess.util;

/**
 * @author - Vitaliy Koshelenko
 *
 * Interface with constants
 *
 */
public interface Constants {
    
    /*====================================================================================================*/
    /*=================================== CONSTANTS FOR CELL VALUES ======================================*/
    /*====================================================================================================*/
    
    int EMPTY   = 0;    //empty cell
    
    int BEATEN  = 1;    //cell, which is beaten by some figure
    
    /*====================================================================================================*/
    /*=============================  CONSTANTS FOR FIGURES CONFIGURATION =================================*/
    /*====================================================================================================*/

    //figure with this configuration option beats other figures on the same row/column
    String LINES        = "lines";

    //figure with this configuration option beats other figures on the same diagonals
    String DIAGONALS    = "diagonals";

    /*====================================================================================================*/
    /*=============================  CONSTANTS FOR MIN/MAX VALUES ========================================*/
    /*====================================================================================================*/    

    int MIN_SIZE = 3;
    int MAX_SIZE = 10;

    int MIN_FIGURES = 0;
    int MAX_FIGURES = 10;

    /*====================================================================================================*/
    /*=============================  CONSTANTS FOR DISPLAY MESSAGES ======================================*/
    /*====================================================================================================*/
    
    String MSG_ROW_COUNT = "Enter rows count: ";
    String MSG_COL_COUNT = "Enter columns count: ";
    String MSG_KING_COUNT = "Enter kings count: ";
    String MSG_ROOKS_COUNT = "Enter rooks count: ";
    String MSG_KNIGHTS_COUNT = "Enter knights count: ";
    String MSG_OFFICERS_COUNT = "Enter officers count: ";
    String MSG_QUEENS_COUNT = "Enter queens count: ";
    
}
