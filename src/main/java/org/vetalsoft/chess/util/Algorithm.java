package org.vetalsoft.chess.util;

/**
 * Enum for algorithm used for finding combinations.
 * 
 * Possible values:
 * 
 *  - org.vetalsoft.chess.util.Algorithm#ITERATION - full board iteration;
 *  - org.vetalsoft.chess.util.Algorithm#ROTATION - 1/4 board iteration and rotation of result combinations.
 * 
 */
public enum Algorithm {
    ITERATION, ROTATION
}
