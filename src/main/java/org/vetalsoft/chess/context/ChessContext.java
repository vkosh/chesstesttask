package org.vetalsoft.chess.context;

import org.vetalsoft.chess.exception.ConfigurationException;
import org.vetalsoft.chess.util.Algorithm;
import org.vetalsoft.chess.util.FigureType;

import java.util.Map;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for chess context.
 * Stores configuration settings. 
 * Initialized in org.vetalsoft.chess.config.ChessConfigurator. 
 */
public class ChessContext {

    /*====================================================================================================*/
    /*========================================= FIELDS ===================================================*/
    /*====================================================================================================*/
    
    /**
     * Map with figure settings.
     *
     * Key - org.vetalsoft.chess.util.FigureType, figure type;
     * Value - java.lang.String, defines beaten cells for figure.
     */
    private static Map<FigureType, String> figuresSettings;
    
    //Algorithm, used for finding combinations
    private static Algorithm algorithm;


    /*====================================================================================================*/
    /*=========================================== PUBLIC METHODS =========================================*/
    /*====================================================================================================*/

    /**
     * Returns configuration settings for specified figure type  
     *
     * @param figureType - FigureType
     * @return - configuration settings for figure
     */
    public static String getFigureConfiguration(FigureType figureType){
        if (figuresSettings == null || !figuresSettings.containsKey(figureType)) {           
            throw new ConfigurationException("Configuration has not been found for figure '" + figureType.name() + "'.");
        }
        return figuresSettings.get(figureType);
    }

    /**
     *
     * @return - Algorithm for finding combinations
     */
    public static Algorithm getAlgorithm() {
        if (algorithm == null) {
            throw new ConfigurationException("Algorithm has not been set yet.");
        }
        return algorithm;
    }

    /*====================================================================================================*/
    /*========================================== PRIVATE METHODS =========================================*/
    /*====================================================================================================*/

    /**
     * Sets figure settings to context.
     * Invoked in org.vetalsoft.chess.config.ChessConfigurator#configure() via reflection API. 
     *    
     * @param figuresSettings - map with figure settings
     */
    private static void setFiguresSettings(Map<FigureType, String> figuresSettings) {
        ChessContext.figuresSettings = figuresSettings;
    }

    /**
     * Sets algorithm to context.
     * Invoked in org.vetalsoft.chess.config.ChessConfigurator#configure() via reflection API.
     * 
     * @param algorithm - Algorithm for finding combinations
     */
    private static void setAlgorithm(Algorithm algorithm) {
        ChessContext.algorithm = algorithm;
    }
}
