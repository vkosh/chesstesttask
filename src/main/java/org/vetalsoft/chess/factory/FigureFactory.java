package org.vetalsoft.chess.factory;

import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.model.impl.*;
import org.vetalsoft.chess.util.FigureType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for generating figure object by figure type (@see org.vetalsoft.chess.model.Constants.ALL_FIGURES)
 */
public class FigureFactory {

    /**
     * Map with figures.
     * Key - figure type (@see org.vetalsoft.chess.util.FigureType)
     * Value - figure instance
     */
    private static Map<FigureType,Figure> figureMap;

    /**
     * Static initialization of figure map
     */
    static {
        figureMap = new HashMap<FigureType, Figure>();
        figureMap.put(FigureType.KING, new KingFigure());
        figureMap.put(FigureType.ROOK, new RookFigure());
        figureMap.put(FigureType.KNIGHT, new KnightFigure());
        figureMap.put(FigureType.OFFICER, new OfficerFigure());
        figureMap.put(FigureType.QUEEN, new QueenFigure());
    }

    /**
     * Returns figure object by it's key 
     *    
     * @param figureType - figure key (@see org.vetalsoft.chess.util.FigureType)
     * 
     * @return - figure object
     */
    public static Figure getFigure(FigureType figureType){
        return figureMap.get(figureType);
    }
    
}
