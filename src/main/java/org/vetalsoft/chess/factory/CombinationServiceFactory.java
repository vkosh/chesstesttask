package org.vetalsoft.chess.factory;

import org.vetalsoft.chess.context.ChessContext;
import org.vetalsoft.chess.service.combination.CombinationsService;
import org.vetalsoft.chess.service.combination.impl.CombinationsServiceIterationImpl;
import org.vetalsoft.chess.service.combination.impl.CombinationsServiceRotationImpl;
import org.vetalsoft.chess.util.Algorithm;

/**
 * @author - Vitaliy Koshelenko
 *
 * Class for obtaining CombinationsService instance.
 */
public class CombinationServiceFactory {
    
    private static CombinationsService _combinationsService; //CombinationsService instance

    /**
     * Returns CombinationsService instance 
     *    
     * @return - CombinationsService instance
     */
    public static CombinationsService getCombinationsService(){
        if (_combinationsService == null) {
            //initialize combinationsService instance
            initializeCombinationsService();
        }
        return _combinationsService;
    }

    /**
     * Initializes CombinationsService instance
     */
    private static void initializeCombinationsService(){
        //Algorithm, used for finding combinations
        Algorithm algorithm = ChessContext.getAlgorithm();
        if (Algorithm.ITERATION.equals(algorithm)) {
            //Use implementation with full iteration
            _combinationsService = new CombinationsServiceIterationImpl();
        } else if (Algorithm.ROTATION.equals(algorithm)) {
            //Use implementation with iteration and board rotation
            _combinationsService = new CombinationsServiceRotationImpl();
        }
    }
    
}
