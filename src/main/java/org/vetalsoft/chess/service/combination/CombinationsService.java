package org.vetalsoft.chess.service.combination;

import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.model.GameField;

import java.util.List;
import java.util.Set;

/**
 * @author - Vitaliy Koshelenko
 *
 * Interface for combination service.
 * Finds set of available combinations.
 */
public interface CombinationsService {

    /**
     * Finds set of available combinations
     *
     * @param rows - rows count
     * @param columns - columns count
     * @param figures - list of figures
     *
     * @return - set of combinations
     */
    Set<GameField> getCombinations(int rows, int columns, List<Figure> figures);
    
}
