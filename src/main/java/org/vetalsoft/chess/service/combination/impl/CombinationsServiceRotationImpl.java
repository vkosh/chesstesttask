package org.vetalsoft.chess.service.combination.impl;

import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.model.GameField;
import org.vetalsoft.chess.service.combination.CombinationsService;
import org.vetalsoft.chess.service.rotation.RotationServiceUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author - Vitaliy Koshelenko
 *
 * Combination service implementation for iteration 1/4 of board and board rotation for found solutions.
 */
public class CombinationsServiceRotationImpl extends CombinationsBaseService implements CombinationsService {

    @Override
    public Set<GameField> getCombinations(int rows, int columns, List<Figure> figures) {
        
        //half of rows
        int rowsToProcess = rows % 2 == 0 ? rows / 2 : rows / 2 + 1;
        //half of columns
        int columnsToProcess = columns % 2 == 0 ? columns / 2 : columns / 2 + 1;

        /**
         * Process 1/4 of board (top left part) 
         */
        Set<GameField> combinations = getCombinations(figures, rows, columns, rowsToProcess, columnsToProcess);

        /**
         * As board is symmetric, new combination can be obtained trough board rotation
         */
        Set<GameField> allCombinations = new HashSet<GameField>();
        for (GameField combination : combinations) {
            allCombinations.add(combination);
            //rotate 180 degrees
            GameField combinationRotated180 = RotationServiceUtil.rotate180degrees(combination);
            allCombinations.add(combinationRotated180);
            //90 and 270 degrees rotation is available only for square boards
            if (rows == columns) {
                //rotate 90 degrees
                GameField combinationRotated90 = RotationServiceUtil.rotate90degrees(combination);
                //rotate 270 degrees
                GameField combinationRotated270 = RotationServiceUtil.rotate270degrees(combination);
                allCombinations.add(combinationRotated90);
                allCombinations.add(combinationRotated270);
            }
        }
        
        return allCombinations;
    }
    
}
