package org.vetalsoft.chess.service.combination.impl;

import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.model.GameField;
import org.vetalsoft.chess.service.combination.CombinationsService;

import java.util.List;
import java.util.Set;

/**
 * @author - Vitaliy Koshelenko
 *
 * Combination service implementation for full iteration.
 */
public class CombinationsServiceIterationImpl extends CombinationsBaseService implements CombinationsService {

    @Override
    public Set<GameField> getCombinations(int rows, int columns, List<Figure> figures) {

        /**
         * This will return all combinations, as the whole board is processed
         */
        return getCombinations(figures, rows, columns, rows, columns);
    }
}
