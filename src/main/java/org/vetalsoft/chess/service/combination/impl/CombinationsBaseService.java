package org.vetalsoft.chess.service.combination.impl;

import org.vetalsoft.chess.model.Cell;
import org.vetalsoft.chess.model.Figure;
import org.vetalsoft.chess.model.GameField;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author - Vitaliy Koshelenko
 *
 * Base class for combination service.
 * Finds set of available combinations.
 */
public abstract class CombinationsBaseService {

    /**
     * Returns set of found combinations for specified rows/columns count (for first figure position) to process
     *
     * @param figures - list of figures
     * @param rows - rows count
     * @param columns - columns count
     * @param rowsToProcess - rows count to process 
     * @param columnsToProcess - columns count to process
     * 
     * @return - set of found combinations
     */
    protected Set<GameField> getCombinations(List<Figure> figures, int rows, int columns, int rowsToProcess, int columnsToProcess) {
        
        //Set of combinations
        Set<GameField> combinations = new HashSet<GameField>();
        Cell startPosition;

        for (int i = 0; i < rowsToProcess; i++) {
            for (int j = 0; j < columnsToProcess; j++) {

                //Move start position for first figure to the next cell on each iteration
                startPosition = new Cell(i, j);

                //Create new combination
                GameField combination = new GameField(rows, columns);

                Set<GameField> stepCombinations = new HashSet<GameField>();

                boolean isParentCombination = true;

                //Iterate over figures
                for (int figureIndex = 0; figureIndex < figures.size(); figureIndex++) {
                    Figure figure = figures.get(figureIndex);
                    int row;  //row index
                    int column; //column index
                    if (figureIndex == 0) {
                        //place first figure at specified position
                        row = startPosition.getRow();
                        column = startPosition.getColumn();

                        //check if figure can be placed
                        if (figure.canBePlaced(combination, row, column)) {
                            //set beaten cells
                            figure.fillBeatenCells(combination, row, column);
                            //set figure cell
                            combination.setValue(row, column, figure.getFigureType().getFigureIndex());

                            //fix for one figure
                            if (figures.size() == 1) {
                                combinations.add(combination);
                            }

                        } else {
                            //can not add figure
                            break;
                        }

                    } else {

                        if (isParentCombination) {

                            //Get child combinations for first combination
                            stepCombinations = getChildCombinations(combination, figure);
                            isParentCombination = false;

                        } else {

                            Set<GameField> allChildCombinations = new HashSet<GameField>();

                            //Get child combinations for each child combination
                            for (GameField childCombination : stepCombinations) {
                                Set<GameField> childCombinations = getChildCombinations(childCombination, figure);
                                allChildCombinations.addAll(childCombinations);
                            }

                            stepCombinations = allChildCombinations;
                        }
                    }
                }

                //add combination to set (if it's valid)
                combinations.addAll(stepCombinations);
            }
        }

        return combinations;
    }

    /**
     * Gets child combination for specified combination and figure
     *
     * @param parentCombination - current combination
     * @param figure - new figure
     *
     * @return - set of child combinations
     */
    protected Set<GameField> getChildCombinations(GameField parentCombination, Figure figure) {

        Set<GameField> childCombinations = new HashSet<GameField>();

        //get free cells
        List<Cell> freeCells = parentCombination.getFreeCells();
        if (freeCells.size() == 0) {
            //No free cells left - can not place figure
            return childCombinations;
        }

        //iterate over free cells
        for (Cell freeCell : freeCells) {
            if (figure.canBePlaced(parentCombination, freeCell.getRow(), freeCell.getColumn())) {
                GameField childCombination = new GameField(parentCombination.getRows(), parentCombination.getColumns(), parentCombination.getGameArray());
                figure.fillBeatenCells(childCombination, freeCell.getRow(), freeCell.getColumn());
                childCombination.setValue(freeCell.getRow(), freeCell.getColumn(), figure.getFigureType().getFigureIndex());
                childCombinations.add(childCombination);
            }
        }

        return childCombinations;
    }
}
