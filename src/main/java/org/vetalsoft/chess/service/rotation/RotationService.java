package org.vetalsoft.chess.service.rotation;

import org.vetalsoft.chess.model.GameField;

/**
 * @author - Vitaliy Koshelenko
 *
 * Interface for rotation service
 */
public interface RotationService {

    /**
     * Rotates board 90 degrees clockwise
     *  
     * @param gameField - board
     * @return - rotated board
     */
    GameField rotate90degrees(GameField gameField);

    /**
     * Rotates board 180 degrees
     *
     * @param gameField - board
     * @return - rotated board
     */
    GameField rotate180degrees(GameField gameField);

    /**
     * Rotates 270 degrees clockwise (90 degrees clock anticlockwise)
     *
     * @param gameField - board
     * @return - rotated board
     */
    GameField rotate270degrees(GameField gameField);
    
}
