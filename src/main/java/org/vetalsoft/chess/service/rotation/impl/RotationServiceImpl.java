package org.vetalsoft.chess.service.rotation.impl;

import org.vetalsoft.chess.model.GameField;
import org.vetalsoft.chess.service.rotation.RotationService;

/**
 * @author - Vitaliy Koshelenko
 *
 * Rotation service implementation
 */
public class RotationServiceImpl implements RotationService {

    /**
     * Rotates board 90 degrees clockwise
     *
     * @param gameField - board
     * @return - rotated board
     */
    @Override
    public GameField rotate90degrees(GameField gameField) {

        int dimension = gameField.getRows();
        int[][] originalArray = gameField.getGameArray();
        
        int[][] rotatedArray = new int[dimension][dimension];
        for (int row = 0; row < dimension; row++) {
            for (int column = 0; column < dimension; column++) {
                rotatedArray[row][column] = originalArray[dimension - column - 1][row];
            }
        }
        
        return new GameField(dimension, dimension, rotatedArray);
    }

    /**
     * Rotates board 180 degrees
     *
     * @param gameField - board
     * @return - rotated board
     */
    @Override
    public GameField rotate180degrees(GameField gameField) {

        int rows = gameField.getRows();
        int columns = gameField.getColumns();
        int[][] originalArray = gameField.getGameArray();

        int[][] rotatedArray = new int[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                rotatedArray[row][column] = originalArray[rows - row- 1][columns - column - 1];
            }
        }

        return new GameField(rows, columns, rotatedArray);
    }

    /**
     * Rotates 270 degrees clockwise (90 degrees clock anticlockwise)
     *
     * @param gameField - board
     * @return - rotated board
     */
    @Override
    public GameField rotate270degrees(GameField gameField) {

        int dimension = gameField.getRows();
        int[][] originalArray = gameField.getGameArray();

        int[][] rotatedArray = new int[dimension][dimension];
        for (int row = 0; row < dimension; row++) {
            for (int column = 0; column < dimension; column++) {
                rotatedArray[row][column] = originalArray[column][dimension - row - 1];
            }
        }

        return new GameField(dimension, dimension, rotatedArray);
    }
}
