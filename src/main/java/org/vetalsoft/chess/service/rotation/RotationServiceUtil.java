package org.vetalsoft.chess.service.rotation;

import org.vetalsoft.chess.model.GameField;
import org.vetalsoft.chess.service.rotation.impl.RotationServiceImpl;

/**
 * @author - Vitaliy Koshelenko
 *
 * Util class for rotation service
 *
 */
public class RotationServiceUtil {

    //Rotation service instance      
    private static RotationService _rotationService;
    
    static {
        //Initialize rotation service instance
        _rotationService = new RotationServiceImpl();
    }

    /**
     * Rotates board 90 degrees clockwise
     *
     * @param gameField - board
     * @return - rotated board
     */
    public static GameField rotate90degrees(GameField gameField){
        return _rotationService.rotate90degrees(gameField);
    };

    /**
     * Rotates board 180 degrees
     *
     * @param gameField - board
     * @return - rotated board
     */
    public static GameField rotate180degrees(GameField gameField){
        return _rotationService.rotate180degrees(gameField);
        
    };

    /**
     * Rotates 270 degrees clockwise (90 degrees clock anticlockwise)
     *
     * @param gameField - board
     * @return - rotated board
     */
    public static GameField rotate270degrees(GameField gameField){
        return _rotationService.rotate270degrees(gameField);
    };
    
}
