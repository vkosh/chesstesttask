package org.vetalsoft.chess.service.figure;

import org.vetalsoft.chess.model.Cell;
import org.vetalsoft.chess.model.GameField;
import org.vetalsoft.chess.service.figure.impl.FigureServiceImpl;

import java.util.List;

/**
 * @author - Vitaliy Koshelenko
 *
 * Util class for figure service
 *
 */
public class FigureServiceUtil {
    
    //Figure service instance      
    private static FigureService _figureService;
    
    static {
        //Initialize figure service instance
        _figureService = new FigureServiceImpl();
    }


    /**
     * Calculates List of beaten cells for figure for specified configuration option
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position
     * @param configurationOption - configuration option for figure (lines, diagonals or row/cell indexes)
     *
     * @return - List of beaten cells
     */
    public static List<Cell> getBeatenCells(GameField gameField, int row, int column, String configurationOption){
        return _figureService.getBeatenCells(gameField, row, column, configurationOption);
    }


    /**
     * Fills beaten cells for figure for specified configuration option
     *
     * @param gameField - GameField
     * @param row - row position
     * @param column - column position
     */
    public static void fillBeatenCells(GameField gameField, int row, int column, String configurationOption){
        _figureService.fillBeatenCells(gameField, row, column, configurationOption);
    };

    
}
