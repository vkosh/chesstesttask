package org.vetalsoft.chess.service.figure.impl;

import org.vetalsoft.chess.model.Cell;
import org.vetalsoft.chess.util.Constants;
import org.vetalsoft.chess.model.GameField;
import org.vetalsoft.chess.service.figure.FigureService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author - Vitaliy Koshelenko
 *
 * Figure service implementation
 *
 */
public class FigureServiceImpl implements FigureService {

    
    /**
     * Calculates List of beaten cells for figure for specified configuration option
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position
     * @param configurationOption - configuration option for figure (lines, diagonals or row/cell indexes)
     *
     * @return - List of beaten cells
     */
    @Override
    public List<Cell> getBeatenCells(GameField gameField, int row, int column, String configurationOption) {

        List<Cell> beatenCells = new ArrayList<Cell>();

        if (Constants.LINES.equalsIgnoreCase(configurationOption.trim())) {
            //add cells on the same row/column
            return getLineCells(gameField, row, column);
        }

        if (Constants.DIAGONALS.equalsIgnoreCase(configurationOption.trim())) {
            //add cells on the same diagonals
            return getDiagonalCells(gameField, row, column);
        }

        //add cell with specified indexes
        String[] configurationIndexes = configurationOption.split(",");
        Integer rowIndex = Integer.valueOf(configurationIndexes[0].trim());
        Integer columnIndex = Integer.valueOf(configurationIndexes[1].trim());
        if (gameField.isValidIndex(row + rowIndex, column + columnIndex)) {
            beatenCells.add(new Cell(row + rowIndex, column + columnIndex));
        }
        return beatenCells;
    }

    /**
     * Calculates List of beaten cells for the same row/column 
     *
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position
     *
     * @return - List of beaten cells
     */
    @Override
    public List<Cell> getLineCells(GameField gameField, int row, int column) {

        List<Cell> beatenCells = new ArrayList<Cell>();

        int columns = gameField.getColumns();
        int rows = gameField.getRows();

        for (int i = 0; i < rows; i++) {
            if (gameField.isValidIndex(i, column) && i != row) {
                beatenCells.add(new Cell(i, column));
            }
        }
        for (int j = 0; j < columns; j++) {
            if (gameField.isValidIndex(row, j) && j != column) {
                beatenCells.add(new Cell(row, j));
            }
        }

        return beatenCells;
    }

    /**
     * Calculates List of beaten cells for the same diagonals
     *
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position
     *
     * @return - List of beaten cells
     */
    @Override
    public List<Cell> getDiagonalCells(GameField gameField, int row, int column) {
        List<Cell> beatenCells = new ArrayList<Cell>();

        int columns = gameField.getColumns();
        int rows = gameField.getRows();

        //diagonal
        int size = columns > rows ? columns : rows;
        for (int i = 0; i < size; i++) {
            if (gameField.isValidIndex(row + i, column + i)) {
                beatenCells.add(new Cell(row + i, column + i));
            }
            if (gameField.isValidIndex(row + i, column - i)) {
                beatenCells.add(new Cell(row + i, column - i));
            }
            if (gameField.isValidIndex(row - i, column + i)) {
                beatenCells.add(new Cell(row - i, column + i));
            }
            if (gameField.isValidIndex(row - i, column - i)) {
                beatenCells.add(new Cell(row - i, column - i));
            }
        }

        return beatenCells;
    }


    /**
     * Fills beaten cells for figure for specified configuration option
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position               
     */
    @Override
    public void fillBeatenCells(GameField gameField, int row, int column, String configurationOption) {

        if (Constants.LINES.equalsIgnoreCase(configurationOption.trim())) {
            fillLineCells(gameField, row, column);
            return;
        }

        if (Constants.DIAGONALS.equalsIgnoreCase(configurationOption.trim())) {
            fillDiagonalCells(gameField, row, column);
            return;
        }

        String[] configurationIndexes = configurationOption.split(",");
        Integer rowIndex = Integer.valueOf(configurationIndexes[0].trim());
        Integer columnIndex = Integer.valueOf(configurationIndexes[1].trim());
        gameField.setValue(row + rowIndex, column + columnIndex, Constants.BEATEN);
    }

    /**
     * Fills beaten cells for figure for the same row/column
     *
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position               
     */
    @Override
    public void fillLineCells(GameField gameField, int row, int column) {

        int columns = gameField.getColumns();
        int rows = gameField.getRows();

        for (int i = 0; i < rows; i++) {
            if (gameField.isValidIndex(i, column) && i != row) {
                gameField.setValue(i, column, Constants.BEATEN);
            }
        }
        for (int j = 0; j < columns; j++) {
            if (gameField.isValidIndex(row, j) && j != column) {
                gameField.setValue(row, j, Constants.BEATEN);
            }
        }
    }

    /**
     * Fills beaten cells for figure for the same diagonals
     *
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position               
     */
    @Override
    public void fillDiagonalCells(GameField gameField, int row, int column) {

        int columns = gameField.getColumns();
        int rows = gameField.getRows();

        //diagonal
        int size = columns > rows ? columns : rows;
        for (int i = 0; i < size; i++) {
            if (gameField.isValidIndex(row + i, column + i)) {
                gameField.setValue(row + i, column + i, Constants.BEATEN);
            }
            if (gameField.isValidIndex(row + i, column - i)) {
                gameField.setValue(row + i, column - i, Constants.BEATEN);
            }
            if (gameField.isValidIndex(row - i, column + i)) {
                gameField.setValue(row - i, column + i, Constants.BEATEN);
            }
            if (gameField.isValidIndex(row - i, column - i)) {
                gameField.setValue(row - i, column - i, Constants.BEATEN);
            }
        }
    }
    
}
