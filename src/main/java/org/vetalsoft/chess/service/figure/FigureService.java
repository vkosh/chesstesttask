package org.vetalsoft.chess.service.figure;

import org.vetalsoft.chess.model.Cell;
import org.vetalsoft.chess.model.GameField;

import java.util.List;

/**
 * @author - Vitaliy Koshelenko
 *
 * Interface for figure service
 *
 */
public interface FigureService {

    /**
     * Calculates List of beaten cells for figure for specified configuration option
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position
     * @param configurationOption - configuration option for figure (lines, diagonals or row/cell indexes)
     *
     * @return - List of beaten cells
     */
    List<Cell> getBeatenCells(GameField gameField, int row, int column, String configurationOption);

    /**
     * Calculates List of beaten cells for the same row/column 
     *
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position
     *
     * @return - List of beaten cells
     */
    List<Cell> getLineCells(GameField gameField, int row, int column);

    /**
     * Calculates List of beaten cells for the same diagonals
     *
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position
     *
     * @return - List of beaten cells
     */
    List<Cell> getDiagonalCells(GameField gameField, int row, int column);

    /**
     * Fills beaten cells for figure for specified configuration option
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position               
     */
    void fillBeatenCells(GameField gameField, int row, int column, String configurationOption);

    /**
     * Fills beaten cells for figure for the same row/column
     *
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position               
     */
    void fillLineCells(GameField gameField, int row, int column);

    /**
     * Fills beaten cells for figure for the same diagonals
     *
     *
     * @param gameField - GameField
     * @param row - row position 
     * @param column - column position               
     */
    void fillDiagonalCells(GameField gameField, int row, int column);
    
}
