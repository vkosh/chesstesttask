package org.vetalsoft.chess.test;

import org.vetalsoft.chess.model.GameField;
import org.vetalsoft.chess.service.rotation.RotationServiceUtil;

public class Rotate180Test {

    public static void main(String[] args) {

        int[][] array = new int[][] {
                {4, 0, 0, 0},
                {0, 3, 0, 0},
                {0, 0, 2, 0}
        };

        GameField gameField = new GameField(3,4,array);
        GameField gameField180 = RotationServiceUtil.rotate180degrees(gameField);

        System.out.println("Original: ");
        gameField.print();

        System.out.println("Rotated 180': ");
        gameField180.print();
        
    }
    
}
