package org.vetalsoft.chess.test;

import org.vetalsoft.chess.model.GameField;
import org.vetalsoft.chess.service.rotation.RotationServiceUtil;

public class RotateTest {

    public static void main(String[] args) {
        
        int[][] array = new int[][] {
            {2, 0, 0},
            {0, 3, 0},
            {4, 0, 0}
        };

        GameField gameField = new GameField(3,3,array);
        GameField gameField90 = RotationServiceUtil.rotate90degrees(gameField);
        GameField gameField180 = RotationServiceUtil.rotate180degrees(gameField);
        GameField gameField270 = RotationServiceUtil.rotate270degrees(gameField);

        System.out.println("Original: ");
        gameField.print();

        System.out.println("Rotated 90': ");
        gameField90.print();

        System.out.println("Rotated 180': ");
        gameField180.print();

        System.out.println("Rotated 270': ");
        gameField270.print();
    }
    
}
